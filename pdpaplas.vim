" Vim syntax file
" Language: pdpaplas
" Maintainer: Seth Price
" Latest Revision: 16 Jun 2021

" NOTE: do not ignore case (for now ig)

" Basically, anything that's not explicitly covered below
syn match aplInvalid "\v.*$"
hi def link aplInvalid Error

" Numbers & radix delimiters
syn match aplNum "\v<(B[01]+|O[0-7]+|D?[0-9]+|X[0-9A-F]+)>" contains=aplNumDel nextgroup=aplInvalid skipwhite
syn match aplNumDel "\v[BOD]" contained
hi def link aplNum Number
hi def link aplNumDel Special

" Special syntax-checking for the MOV command, which needs two arguments
syn match aplNumTwo "\v<(B[01]+|O[0-7]+|D?[0-9]+|X[0-9A-F]+),(B[01]+|O[0-7]+|D?[0-9]+|X[0-9A-F]+)>" contains=aplArgComma skipwhite
syn match aplArgComma "\v," contained
hi def link aplNumTwo Number
hi def link aplArgComma Operator

" And for the DRP command, which also needs two (but separated with a space)
syn match aplNumThen "\v<(B[01]+|O[0-7]+|D?[0-9]+|X[0-9A-F]+)>" contains=aplNumDel nextgroup=aplNum skipwhite
hi def link aplNumThen Number

" Directives
syn keyword aplCmd NOP nextgroup=aplInvalid skipwhite
syn keyword aplCmd MOV nextgroup=aplNumTwo skipwhite
syn keyword aplCmd DRW nextgroup=aplNum skipwhite
syn keyword aplCmd DRP nextgroup=aplNumThen skipwhite
syn keyword aplCmd END nextgroup=aplInvalid skipwhite
syn keyword aplCmd CLR nextgroup=aplInvalid skipwhite
syn keyword aplCmd INV nextgroup=aplInvalid skipwhite
syn keyword aplCmd EOF nextgroup=aplInvalid skipwhite
hi def link aplCmd Statement

" Comments
setlocal comments="/"
syn match aplComment "\v^\s*/.*$" contains=@Spell,aplTodo
syn match aplTodo "\v(TODO|FIXME|XXX|NOTE)" contained
hi def link aplComment Comment
hi def link aplTodo Todo
