/**
 * pdpaplasm - pdpapl drawing language assembler
 * Copyright 2021 Seth Price, all rights reserved.
 * See `LICENSE' file for copyright information.
 */

%{
	#include <string.h>
	#include "pdpaplas.tab.h"

	#define YYSTYPE long
%}

%option noyywrap
%option caseless
%option yylineno

%%

\/.*\n		{ /* comment */ }

[0-9]+		{ yylval = strtoul(yytext, NULL, 10); return NUMBER; }
B[01]+		{ yylval = strtoul(++yytext, NULL, 2); return NUMBER; }
O[0-7]+		{ yylval = strtoul(++yytext, NULL, 8); return NUMBER; }
D[0-9]+		{ yylval = strtoul(++yytext, NULL, 10); return NUMBER; }
X[0-9A-F]+	{ yylval = strtol(++yytext, NULL, 16); return NUMBER; }

,		{ return COMMA; }
[\r\n]		{ return SEPARATOR; }

NOO?P		{ return INSTR_NOP; }
MOVE?		{ return INSTR_MOV; }
DRA?W		{ return INSTR_DRW; }
DR(A?W)?P	{ return INSTR_DRP; }
END		{ return INSTR_END; }
INV		{ return INSTR_INV; }
CLR		{ return INSTR_CLR; }

[ \t]*		{ /* whitespace */ }
%%
