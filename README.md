pdpapl Drawing Language Assembler
=================================

Converts human-readable terminal graphics drawing instructions into a
MACRO-11 file containing raw binary data to be compiled into the
[pdpapl](https://www.gitlab.com/ssterling/pdpapl/) program.

Unlike pdpapl itself, this is written in Flex/Bison and is intended to be run
on a modern POSIX-compliant system.
One project in MACRO-11 was more than enough for me.

Compilation & usage
-------------------

```console
$ make
$ ./pdpaplas /path/to/input.apl /path/to/output.mac
```

How you get this into the working directory is up to you.
If you’re on an emulator, the easiest way would likely be opening up `kex`
and pasting the entire file into the terminal emulator.

File syntax
-----------

Statements are separated by newlines.
Numbers may be prefixed with either `B` to indicate binary, `O` for octal,
`D` for decimal, or `X` for hexadecimal; numbers lacking a prefix
will be read as decimal.

* `NOP` — Do nothing.
* `MOV 𝑥,𝑦` — Move the cursor to a given 𝑥,𝑦 (decimal) pixel position
  on the screen, where the top left pixel is 0,0 (i.e. starts couting at 0).
* `DRW 𝑛𝑛𝑛𝑛…` — Sequentially draw the specified pixels starting at the
  current cursor position.
  For each `1` bit, a character will be drawn; for each `0` bit, a character
  will be erased.
  Any `0` bits leading up to the first `1` bit will be discarded.
  The number of bits after and including the initial `1` bit may be
  an even number from 2 to 30.
* `DRP 𝑐 𝑛𝑛𝑛𝑛…` — Functions like the `DRW` command, but writes exactly 𝑐
  crumbs, left-padding with zeroes (i.e. does not discard leading `0` bits).
* `END` — Marks end of frame; tells the video processor to not continue
  drawing till the Line Time Clock signals that 1÷FPS seconds has passed
  since the frame started to be drawn.
* `INV` — Inverts black and white on the terminal screen (reverse video).
* `CLR` — Clear the screen to the background colour.

Comments begin with a `/` and must be on their own line.

### A note on usage

This program was originally intended as an extremely ad-hoc method for
preserving my sanity when recreating an entire 484 frames of animation
from scratch.
However, upon compiling test programs consisting of the instructions to
draw a smiley on the screen, it occured to me that pdpapl can be used to
draw virtually any 40×24px, ≈2.2fps animation one desires (within reason).
I doubt anyone else out there really wants to be outputting low-res ASCII art
on a VT102, but I just thought it’d be worth mentioning.

Bugs
----

Please report any bugs to the
[issue tracker](https://gitlab.com/ssterling/pdpaplasm/issues).
