all: pdpaplas

pdpaplas.tab.c pdpaplas.tab.h: pdpaplas.y
	bison -d pdpaplas.y

lex.yy.c: pdpaplas.l pdpaplas.tab.h
	flex pdpaplas.l

pdpaplas: lex.yy.c pdpaplas.tab.c pdpaplas.tab.h
	cc -g lex.yy.c pdpaplas.tab.c -o pdpaplas

.PHONY: clean
clean:
	rm -rf pdpaplas.tab.c pdpaplas.tab.h lex.yy.c pdpaplas
